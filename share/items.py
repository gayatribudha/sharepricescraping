# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class ShareItem(scrapy.Item):
    TradedCompanies = scrapy.Field()
    NoOfTransaction = scrapy.Field()
    MaxPrice = scrapy.Field()
    MinPrice = scrapy.Field()
    ClosingPrice = scrapy.Field()
    TradedShares = scrapy.Field()
    Amount = scrapy.Field()
    PreviousClosing = scrapy.Field()
    DifferenceRs = scrapy.Field()
    ScrappedTime = scrapy.Field()


