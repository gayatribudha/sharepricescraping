import scrapy
from scrapy import Spider
from scrapy.selector import Selector
from share.items import ShareItem
from datetime import datetime
import json

class ShareSpider(Spider):
    name = "shares"
    
    def start_requests(self):
        urls = [
            'http://nepalstock.com.np/main/todays_price/index/{}/'.format(i) for i in range(1, 9)
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
      
    def parse(self, response):
        for row in response.xpath('//*[@class="table table-condensed table-hover"]//tr'):
              item = ShareItem()

              if row.xpath('td[1]//text()').extract_first() == "Total Amount Rs.":
                continue
              elif row.xpath('td[1]//text()').extract_first() == "Total Quantity":
                continue
              elif row.xpath('td[1]//text()').extract_first() == "Total No of Transactions":
                continue
              elif row.xpath('td[1]//text()').extract_first() == "S.N.":
                continue
              elif row.xpath('td[1]//text()').extract_first() == "\n            ":
                continue
              elif row.xpath('td[2]//text()').extract_first() is None:
                continue

              item['ScrappedTime'] = datetime.now().strftime('%Y-%m-%d %H:%M')
              item['TradedCompanies'] = row.xpath('td[2]//text()').extract_first()
              item['NoOfTransaction'] = row.xpath('td[3]//text()').extract_first() 
              item['MaxPrice'] = row.xpath('td[4]//text()').extract_first()
              item['MinPrice'] = row.xpath('td[5]//text()').extract_first()
              item['ClosingPrice'] = row.xpath('td[6]//text()').extract_first() 
              item['TradedShares'] = row.xpath('td[7]//text()').extract_first()
              item['Amount'] = row.xpath('td[8]//text()').extract_first()
              item['PreviousClosing'] = row.xpath('td[9]//text()').extract_first() 
              item['DifferenceRs'] = row.xpath('td[10]//text()').extract_first().rstrip()
              
            #   with open('sharePrices.json', 'a', encoding='utf-8') as file:
            #     file.write(json.dumps(item) + '\n')

              yield item
        
    
           