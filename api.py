from flask import Flask
from flask import jsonify
from flask import request
from flask_pymongo import PyMongo
from pymongo import MongoClient
from bson.json_util import dumps
from flask import jsonify
import json

app = Flask(__name__)

client = MongoClient('localhost:27017')
db = client.SharePriceInfo
app.config['MONGO_URI'] = 'mongodb://localhost:27017/SharePriceInfo'

api = PyMongo(app)

@app.route('/sharePrice', methods=['GET'])
def get_all_sharePrices():
    sharePrice = api.db.NepalSharePrices
    output = []

    for info in sharePrice.find():
        output.append(
            {'Scrapped Time': info['ScrappedTime'],
            'Traded Companies': info['TradedCompanies'],
            'No Of Transaction': info['NoOfTransaction'],
            'Max Price': info['MaxPrice'],
            'Min Price': info['MinPrice'],
            'Closing Price': info['ClosingPrice'],
            'Amount': info['Amount'],
            'Previous Closing': info['PreviousClosing'],
            'Difference Rs': info['DifferenceRs']
            }
        )
    return jsonify({'SharePriceInfo': output})

    

if __name__ == '__main__':
    app.run(debug=True)


    