### Scraping the share prices data from the Nepal Stock website.

#### Features
- Scrap the share prices information from the Nepal Stock website 
- The data are scraped from the today's share price table from all 1 t0 9 pages in each minute
- The scrapped data are stored in mongo database with name SharePriceInfo
- There has also been made a small API with flask to retrieve all the stored data from the mongodb in json format.
 
#### Requirements
- All the requirements for running the application are available in requirements.txt file
    - ```pip install -r /path/to/requirements.txt```

- Few of the application requirement highlights
    - Scrapy
    - Apscheduler
    - Pymongo
    - Mongodb
    - Flask-PyMongo
    - Postman

#### Instruction to run the program
- First, all the required libraries must be installed
- Then, enter ```python scheduler.py``` in terminal
- This will scrap the data from the website to the mongodb directly


#### See the scrapped data inside mongo db
- Since data are directly stored inside mongo database
- We can see the records inside mongo db from terminal as well
- Enter into mongo shell entering ```mongo``` in the terminal inside same share directory
- After entering into mongo shell, ```show databases``` will list all the databases in the mongo
- Since our database name is SharePriceInfo enter ```use SharePriceInfo``` to use that particular database
- Then, enter ```show collections``` which will list all the collections in that database
- Know that NepalSharePrice is where all the records have been stored.
- At last enter ```db.NepalSharePrices.find().pretty()``` to retrive all the stored records in NepalSharePrices collection
- To exit from mongo enter ```exit```



#### Instruction to run the API
- api.py files contains a small api built from flask to retrieve all the stores records fromt he mongo db
- Enter ```python api.py``` in terminal to run the api
- Open postman and enter the ```http://127.0.0.1:5000/sharePrice``` to get the data 
- The retireved data can be well formated using json format in postman