from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from apscheduler.schedulers.twisted import TwistedScheduler

from share.spiders.shares_spider import ShareSpider
from datetime import datetime


process = CrawlerProcess(get_project_settings())
scheduler = TwistedScheduler()
scheduler.add_job(process.crawl, 'interval', args=[ShareSpider], seconds=60)
scheduler.start()
process.start(False)



